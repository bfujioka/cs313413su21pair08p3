package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import java.util.Iterator; //consider removal
import java.util.List; //consider removal
import edu.luc.etl.cs313.android.shapes.model.*;
import java.util.ArrayList;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas;
		this.paint = paint;
		paint.setStyle(Style.STROKE);
		//FIXED to not null.
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
		//Don't change anything
	}

	@Override
	public Void onStrokeColor(final StrokeColor c) {
		int colorStroke = paint.getColor();
		paint.setColor(c.getColor());
		c.getShape().accept(this);
		paint.setColor(colorStroke);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
		Style fill = paint.getStyle();
		paint.setStyle(Style.FILL_AND_STROKE);
		f.getShape().accept(this);
		paint.setStyle(fill);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {

		for (Shape s: g.getShapes()) {
			s.accept(this); }
		return null;
	}



	@Override
	public Void onLocation(final Location l) {
		canvas.translate(l.getX(), l.getY());
		l.getShape().accept(this);
		canvas.translate(-l.getX(), -l.getY());
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0,0,r.getWidth(), r.getHeight(), paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		Style beforeThis = paint.getStyle();
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(beforeThis);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
		//There's likely a shorter way of performing this with an iterator and data structure.
		int a = s.getPoints().size();
		float[] points = new float[4 * a];
		int b = 0;
		for (Point point : s.getPoints()) {
			points[b] = point.getX();
			points[b + 1] = point.getY();
			b += 4;
		}

		for (int i = 4; i <4 * a; i += 4) {
			points[i - 2] = points[i];
			points[i - 1] = points[i + 1];
		}

		points[4 * a - 2] = points[0];
		points[4 * a - 1] = points[1];

		canvas.drawLines(points, paint);
		return null;
	}
}
