package edu.luc.etl.cs313.android.shapes.model;
import java.util.List;
import android.graphics.Canvas;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		int radius = c.getRadius(); //remove final
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		return f.getShape().accept(this); //write code here
	}

	@Override
	public Location onGroup(final Group g) {

		int xLeft = Integer.MAX_VALUE;
		int xRight =  Integer.MIN_VALUE;
		int yDown = Integer.MAX_VALUE;
		int yUp = Integer.MIN_VALUE;
		for (Shape shape : g.getShapes()) {
			Location location = shape.accept(this);
			Rectangle rectangle = (Rectangle) location.getShape();
			xLeft = Math.min(xLeft, location.getX());
			xRight = Math.max(xRight, location.getX() + rectangle.getWidth());
			yDown = Math.min(yDown, location.getY());
			yUp = Math.max(yUp, location.getY() + rectangle.getHeight());
		}
		return new Location(xLeft, yDown, new Rectangle(xRight - xLeft, yUp - yDown));
		//remove final



	}

	@Override
	public Location onLocation(final Location l) {
		Location location = l.shape.accept(this);
		return new Location(l.x+location.x, l.y+location.y, location.shape);

	}

	@Override
	public Location onRectangle(final Rectangle r) {

		return new Location(0, 0, new Rectangle(r.getWidth(), r.getHeight()));
	}

	@Override
	public Location onStrokeColor(final StrokeColor c) {
		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {
		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {
		return onGroup(s);
	}
	}
